package com.example;

import org.hibernate.validator.ClassValidator;
import org.hibernate.validator.InvalidValue;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ResourceBundle;

public class MyClassTest {
    private MyClass sut = new MyClass("1234567890");
    private ResourceBundle bundle;

    @Before
    public void setup() {
        sut = new MyClass("1234567890");
        bundle = ResourceBundle.getBundle("ValidationMessages");
    }

    @Test
    public void validateNameTest() {
        sut.setName("1234567890abcdef");
        sut.setDescription("12345");

        // get validator for class and validate it
        ClassValidator<MyClass> validator = new ClassValidator<MyClass>(MyClass.class);
        InvalidValue[] invalidValues = validator.getInvalidValues(sut);

        // check validation results
        Assert.assertEquals(1, invalidValues.length);
        Assert.assertEquals("Name should be 1 to 10 characters long.", invalidValues[0].getMessage());
    }

    @Test
    public void validateDescriptionTest() {
        sut.setName("correct");
        sut.setDescription("way too many characters");

        // get validator for class and validate it
        ClassValidator<MyClass> validator = new ClassValidator<MyClass>(MyClass.class);
        InvalidValue[] invalidValues = validator.getInvalidValues(sut);

        // check validation results
        Assert.assertEquals(1, invalidValues.length);
        Assert.assertEquals("Description must be between 1 and 10 characters long.", invalidValues[0].getMessage());
    }

    @Test
    public void accessBundleTest() {
        Assert.assertEquals("Description must be between 1 and 10 characters long.", bundle.getString("descriptionMsg"));
    }
}
