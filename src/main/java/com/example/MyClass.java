package com.example;

import org.hibernate.validator.Length;

public class MyClass {
    @Length(min = 1, max = 10, message = "Name should be 1 to 10 characters long.")
    private String name;

    @Length(min = 1, max = 10, message ="{descriptionMsg}")
    private String description;

    public MyClass(String name) {
        this.name = name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
